import {
  Typography,
  Table,
  Row,
  Col,
  Space,
  Button,
  Input,
  Popconfirm,
  Checkbox,
} from "antd";
import { useDiscounts } from "../../hooks/useDiscount";
import ModalDiscount from "../../components/Modal";

const { Title } = Typography;
const { Search } = Input;

const Management = () => {
  const {
    visibleModalDiscount,
    dataSourceDiscounts,
    showModalDiscount,
    closeModalDiscount,
    deleteDiscount,
    editDiscount,
    filterDiscount,
    handleBlurFilterInput,
    filterVehicleType,
  } = useDiscounts();

  const tableColumns = [
    {
      title: "Marca",
      dataIndex: "marca",
    },
    {
      title: "Ano do modelo",
      dataIndex: "modelo",
    },
    {
      title: "Ano de fabricação",
      dataIndex: "ano",
    },
    {
      title: "Cor",
      dataIndex: "cor",
    },
    {
      title: "Tipo de cambio",
      dataIndex: "tipo",
    },
    {
      title: "Ação",
      dataIndex: "action",
      with: 80,
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            type="primary"
            onClick={() => editDiscount(record.key)}
          >
            Editar
          </Button>

          <Popconfirm
            title="Deseja realmente deletar?"
            onConfirm={() => deleteDiscount(record.key)}
          >
            <Button type="primary" danger>
              Deletar
            </Button>
          </Popconfirm>
        </Space>
      ),
    }
  ];

  return (
    <>
      <Row style={{ textAlign: "center", margin: "32px 16px" }}>
        <Col span={24}>
          <Title level={3}>Veiculos cadastrados</Title>
        </Col>

        <Col
          span={24}
          style={{
            margin: "16px 0",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Button type="primary" onClick={showModalDiscount}>
            Adicionar veiculo
          </Button>

          <Space>
            <Checkbox.Group onChange={(check) => filterVehicleType(check)}>
              <Checkbox value="automatico">
                Automaticos
              </Checkbox>
              
              <Checkbox value="manual">
                Manuais
              </Checkbox>
            </Checkbox.Group>

            <Search
              placeholder="pesquise aqui"
              allowClear
              style={{ width: 200 }}
              onChange={(event) => filterDiscount(event.target.value)}
              onBlur={handleBlurFilterInput}
            />
          </Space>
        </Col>

        <Col span={24}>
          <Table
            columns={tableColumns}
            dataSource={dataSourceDiscounts}
          />
        </Col>

        <ModalDiscount
            visible={visibleModalDiscount}
            onCancel={closeModalDiscount}
            text="Adicionar oferta"
          />
      </Row>
    </>
  );
};

export default Management;
