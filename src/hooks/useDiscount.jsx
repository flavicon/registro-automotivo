import { message, Form } from "antd";
import moment from "moment";
import { createContext, useContext, useEffect, useState } from "react";
import {
  collection,
  getDocs,
  addDoc,
  setDoc,
  deleteDoc,
  doc,
} from "firebase/firestore/lite";
import { db } from "../services/firebase";
import _ from 'lodash';

const DiscountContext = createContext({});

export function DiscountProvider({ children }) {
  const [discounts, setDiscounts] = useState([]);
  const [dataSourceDiscounts, setDataSourceDiscounts] = useState([]);
  const [discountInEdition, setDiscountInEdition] = useState("");
  const [filterSearch, setFilterSearch] = useState(false);
  const [visibleModalDiscount, setVisibleModalDiscount] = useState(false);
  const [confirmModalLoading, setConfirmModalLoading] = useState(false);

  const [formDiscount] = Form.useForm();

  const showModalDiscount = () => setVisibleModalDiscount(true);
  const closeModalDiscount = () => {
    setVisibleModalDiscount(false);
    setDiscountInEdition("");
  };

  const searchDiscounts = async () => {
    const instaceDiscountCollection = collection(db, "ofertas");
    const discountsSnapshot = await getDocs(instaceDiscountCollection);
    const discountsList = discountsSnapshot.docs.map((doc) => {
      const discountItem = doc.data();
      const discountsData = {
        ...discountItem,
        key: doc.id,
      };
      return discountsData;
    });
    setDiscounts(discountsList);
    setDataSourceDiscounts(discountsList);
  };

  useEffect(() => {
    searchDiscounts();
  }, []);

  const createDiscunt = async (discount) => {
    if (discountInEdition !== "") {
      updateDiscount(discount);
      return;
    }

    const discountAlreadyExists = discounts.filter((discountRegistered) => {
      delete discountRegistered.key;

      const isEquals = _.isEqual(discountRegistered, discount);

      return isEquals;
    });

    if (0 < discountAlreadyExists.length) {
      message.error('Veiculo ja cadastrado.');

      return;
    }

    try {
      setConfirmModalLoading(true);

      await addDoc(collection(db, "ofertas"), discount).then(() => {
        setConfirmModalLoading(false);
        setVisibleModalDiscount(false);
      });

      message.success("Veiculo cadastrado com sucesso!");
      searchDiscounts();
    } catch {
      message.error("Problemas ao gravar dados, tente novamente.");
    }
  };

  const updateDiscount = async (discount) => {
    setConfirmModalLoading(true);
    await setDoc(doc(db, "ofertas", discountInEdition), discount).then(() => {
      setConfirmModalLoading(false);
      setVisibleModalDiscount(false);
      setDiscountInEdition("");
    });
    message.success("atualizado com sucesso.");
    searchDiscounts();
  };

  const deleteDiscount = async (discountId) => {
    try {
      await deleteDoc(doc(db, "ofertas", discountId));
      message.success("Oferta excluida com sucesso.");
      searchDiscounts();
    } catch {
      message.error("Problemas ao excluir dados, tente novamente.");
    }
  };

  const editDiscount = (discountId) => {
    showModalDiscount();
    setDiscountInEdition(discountId);

    const discount = discounts.filter((item) => item.key === discountId);
    const objectDiscount = { ...discount[0] };

    formDiscount.setFieldsValue({
      ...objectDiscount,
      ano: moment(objectDiscount?.ano),
      modelo: moment(objectDiscount?.data),
    });
  };

  const filterDiscount = (filter) => {
    if (filter === "") searchDiscounts();

    const filtered = dataSourceDiscounts.filter(
      (item) =>
        item.marca.toLowerCase().includes(filter.toLowerCase()) ||
        item.modelo.toLowerCase().includes(filter.toLowerCase()) ||
        item.placa.toLowerCase().includes(filter.toLowerCase())
    );

    if (filtered.length > 0) {
      setDataSourceDiscounts(filtered);
      setFilterSearch(true);
    }
  };

  const filterVehicleType = (filter) => {
    if (0 === filter.length) {
      searchDiscounts();

      return;
    }

    const vehicles = discounts.filter((vehicle) => filter.includes(vehicle.tipo));
    
    setDataSourceDiscounts(vehicles);
    setFilterSearch(true);
  }

  const handleBlurFilterInput = () => {
    setFilterSearch(false);
  };

  if (!filterSearch && dataSourceDiscounts.length < discounts.length)
    setDataSourceDiscounts(discounts);

  return (
    <DiscountContext.Provider
      value={{
        discounts,
        setDiscounts,
        dataSourceDiscounts,
        createDiscunt,
        visibleModalDiscount,
        showModalDiscount,
        closeModalDiscount,
        confirmModalLoading,
        deleteDiscount,
        editDiscount,
        filterDiscount,
        formDiscount,
        filterSearch,
        handleBlurFilterInput,
        filterVehicleType,
      }}
    >
      {children}
    </DiscountContext.Provider>
  );
}

export function useDiscounts() {
  const context = useContext(DiscountContext);
  return context;
}
