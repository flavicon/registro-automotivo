import {
  Modal,
  Form,
  Input,
  DatePicker,
  Row,
  Col,
  Select
} from "antd";
import moment from "moment";
import { useDiscounts } from "../../hooks/useDiscount";

import './styles.scss';

const { Option } = Select;

const ModalDiscount = (props) => {
  const { createDiscunt, confirmModalLoading, formDiscount } = useDiscounts();

  const handleOk = () => {
    formDiscount.validateFields().then((values) => {
      const formFieldsFormated = {
        ...values,
        ano: moment(values.ano).format("YYYY"),
        modelo: moment(values.ano).format("YYYY"),
        visualizacoes: 0,
      };

      createDiscunt(formFieldsFormated);
    });
  };

  return (
    <Modal
      title="Adicionar nova oferta"
      visible={props.visible}
      onOk={handleOk}
      confirmLoading={confirmModalLoading}
      onCancel={props.onCancel}
      centered
      destroyOnClose
    >
      <Form
        form={formDiscount}
        layout="vertical"
        preserve={false}
      >
        <Row gutter={[16, 8]}>
          <Col span={12}>
            <Form.Item
              name="marca"
              label="Marca"
              rules={[
                {
                  required: true,
                  message: "Por favor insira a marca do veículo.",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
          </Col>

          <Col span={12}>
            <Form.Item
              name="cor"
              label="Cor"
              rules={[
                {
                  required: true,
                  message: "Por favor selecione a cor do veículo.",
                },
              ]}
            >
              <Select size="large">
                <Option value="branco">Branco</Option>
                <Option value="cinza">Cinza</Option>
                <Option value="preto">Preto</Option>
                <Option value="vermelho">Vermelho</Option>
              </Select>
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item
              name="tipo"
              label="Tipo de cambio"
              rules={[
                {
                  required: true,
                  message: "Por favor selecione o tipo de cambio.",
                },
              ]}
            >
              <Select size="large">
                <Option value="automatico">Automatico</Option>
                <Option value="manual">Manual</Option>
              </Select>
            </Form.Item>
          </Col>

          <Col span={12}>
            <Form.Item
              name="modelo"
              label="Ano do modelo"
              rules={[
                {
                  required: true,
                  message: "Por favor insira o modelo do veículo.",
                },
              ]}
            >
              <DatePicker
                size="large"
                picker="year"
                placeholder="Selecione o ano"
              />
            </Form.Item>
          </Col>

          <Col span={12}>
            <Form.Item
              name="ano"
              label="Ano de fabricação"
              rules={[
                {
                  required: true,
                  message: "Por favor insira o ano do veículo.",
                },
              ]}
            >
              <DatePicker
                size="large"
                picker="year"
                placeholder="Selecione o ano"
              />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item
              name="foto"
              label="Foto"
              tooltip="Insira um link formato url."
              rules={[
                {
                  type: "url",
                  required: true,
                  message: "Por favor insira um link url válido.",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default ModalDiscount;
