import React from "react";
import { Space, Card, List, Divider, Tag } from "antd";
import { EyeOutlined } from "@ant-design/icons";

import "./styles.scss";

const { Meta } = Card;

const IconText = ({ icon, text }) => (
  <Space>
    {React.createElement(icon)}
    {text}
  </Space>
);

const CardGrid = (props) => {
  return (
    <List
      itemLayout="horizontal"
      grid={{ gutter: 16, column: 3 }}
      size="large"
      dataSource={props.dataSource}
      renderItem={(item) => (
        <List.Item
          onClick={() => props.onClick(item)}>
          <Card
            hoverable
            style={{ width: 300 }}
            title={item.marca}
            cover={<img alt="car" src={item.foto} />}
          >
            <Meta
              title="Oferta especial"
              description="Confira está grande oportunidade que temos para você. 
              Veiculos altamente confortáveis, com quatro rodas, cinco assentos
                e o valor naquele preço."
            />
            <Divider />
            <Space>
              <Tag color="cyan">Marca: {item.marca}</Tag>
              <Tag color="blue">Ano / Modelo: {item.ano} / {item.modelo}</Tag>
            </Space>
            <Divider />
            <IconText
              icon={EyeOutlined}
              text={`${item.visualizacoes} visualizações`}
            />
          </Card>
        </List.Item>
      )}
    />
  );
};

export default CardGrid;
