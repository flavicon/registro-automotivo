# Registro automotivo

# Summary

[1. Installing the project](#instalation)

[2. Coding on the project](#coding)

[3. Testing](#test)

# Instalation

- How i can install this project?

> In your terminal run:

```shell
git clone https://gitlab.com/flavicon/registro-automotivo.git

cd registro-automotivo

npm install
```

# Coding

- How create a new branch?

> In `master` branch on local repo, run:

```shell
git checkout -b issue_type/branch_name
```

### Issue types

- Feature: `feat`

- Bug: `fix`

- Enhancement: `enhancement`

### Branch name partner

> The branch name must be `issue_type` + `/` + `issue_description`

_E.g._

- Feature: `feat/create_dashboard_page`

- Bug: `fix/schedule_modal`

- Enhancement: `enhancement/add_schedule_button`


# Test

- How i can to test?

```shell
cd registro-automotivo

npm run cypress:open
```
